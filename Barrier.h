#ifndef BARRIER_H
#define BARRIER_H

#include <atomic>
#include <mutex>
#include <iostream>
#include <condition_variable>

using namespace std;
//Class created in FEE CTU course PAG
class Barrier {
private:
    atomic<int> in_barrier;
    atomic<int> phase_cnt;
    atomic<bool> master_switch;
    atomic<bool> quit_switch;
    int nm;

    mutex mtx_master;
    condition_variable cond_master;
    mutex mtx_slave;
    condition_variable cond_slave;
public:
    Barrier(int slave_threads);

    bool get_to_quit();

    void wake_slaves(bool to_quit);

    void wait_master();

    void wait_slave();

    void acquire_lock_slave();

    void unlock_lock_slave();
};

#endif
