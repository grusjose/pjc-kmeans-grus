//
// Created by josef on 05.10.20.
//

#include "DataPoint.h"
#include "random"
#include "k_means_computation.h"
#include "Barrier.h"
#include <ostream>
#include <sstream>
#include <fstream>
#include <iostream>
#include <thread>


K_means_memory compute_k_means_single_threaded(PointMemory& points, int k, int dim, int cycles, int epochs, double tolerance){
    /*
     *  single thread variant acts in linear style, every iteration computes assignments of points to saved means,
     *  then updates that means to lie in their arithmetic mean
     */
    std::default_random_engine generator;
    auto distro = std::uniform_int_distribution<int>(0, points.number_of_points - 1);
    K_means_memory final_mem(k, dim);

    double min_loss = 0;
    int bound = (int)(points.number_of_points / ((double) k * 20)); // initial mean distribution parameter

    for (int epoch = 0; epoch < epochs; epoch++) {   // every epoch starts with new reinitialization
        //initialization
        auto main_mem = K_means_memory(k, dim);
        auto agg_mem = K_means_memory(k, dim);
        auto* mem_p = &main_mem;
        auto* agg_p = &agg_mem;

        //using pointers to reuse 2 kmeans memory objects every cycle

        initialize_point_by_random(0, *mem_p, points);
        mem_p->set_lim(1);
        for (int i = 1; i < k; i++){
            initialize_point_by_random(i, *mem_p, points); // more pure variant, initialize means by random

            /*  // initialize first point by random, other points by finding distant other point
             * currently not used due to lack of randomness
             */
            /*
            agg_p->reset_memory();
            assign_points_and_accumulate_coords(*mem_p, *agg_p, points, 0, points.number_of_points);
            initialize_point_from_mem(i, agg_p->get_most_distant_point(), *mem_p, points);
            mem_p->set_lim(i+1);
            */
        }
        mem_p->set_lim(k);
        for (int cycle = 0; cycle < cycles; cycle++) {
            // iterate until stopping criterion is met or run out of cycles

            assign_points_and_accumulate_coords(*mem_p, *agg_p, points, 0, points.number_of_points);
            /*
             * create assignment, accumulate points to corresponding means
             */
            int to_init = 0;
            for (int i = 0; i < k; i++) {
                auto b = agg_p->average_accumulated(i, bound);
                    /*
                     *  bound stops to create classes with too little assignend points
                     *  otherwise reinitialized such mean
                     */
                if (!b) {
                    //if (to_init == 0) initialize_point_from_mem(i, agg_p->get_most_distant_point(), *agg_p, points);
                    //else initialize_point_by_random(i, *agg_p, points);
                    initialize_point_by_random(i, *agg_p, points);
                    to_init++;
                }
            }
            //compute error
            auto error = mem_p->get_max_change(*agg_p);
            auto tmp = mem_p;
            mem_p = agg_p;
            agg_p = tmp;
            //change roles of buffers -- mem contains current means, agg contains aggregated means in new iteration
            if (error < tolerance) {
                // break from cycle when criterion is met
                break;
            }
        }
        if (mem_p->get_L2loss() < min_loss || epoch == 0) {
            // save only best set of means based on L2 loss to most distant point
            final_mem.copy_data(*mem_p);
            min_loss = mem_p->get_L2loss();
        }
    }
    K_means_memory tmp(k, dim);
    assign_points_and_accumulate_coords(final_mem, tmp, points, 0, points.number_of_points);
    // final reassignment and return results
    return final_mem;
}

void compute_k_means_parallel_slave(PointMemory* points, int left, int right, K_means_memory** current_mem,
                                  K_means_memory* agg_mem, Barrier& barrier, int idx){
    /*
     *  slave thread procedure
     *  whenever signalized, compute assignments for its part of point memory and accumulate new means values in its local
     *  agg_mem
     *  last slave thread to synchronize using barrier signalizes master to recompute means
     */
    while(true){
        K_means_memory copy_mem = K_means_memory(**current_mem);
        assign_points_and_accumulate_coords(copy_mem, *agg_mem, *points, left, right);
        if (barrier.get_to_quit()) break;
        barrier.wait_slave();
    }
}


K_means_memory compute_k_means_parallel_master(PointMemory& points, int k, int dim, int cycles, int epochs, double tolerance, int nm_of_threads){
    vector<thread> workers;
    vector<K_means_memory*> thread_data;

    double min_loss=0;
    int bound = (int) (points.number_of_points / ((double) k * 20));

    K_means_memory final_mem(k, dim);

    auto main_mem = K_means_memory(k, dim);
    auto agg_mem = K_means_memory(k, dim);
    int step = points.number_of_points / nm_of_threads;
    int left = 0, right = step;
    auto* mem_p = &main_mem;
    auto* agg_p = &agg_mem;
    K_means_memory** delegated = &mem_p; // enables to keep reusing two k means memories in master thread and pass them to slaves
    Barrier barrier(nm_of_threads);
    for (int workerIndex = 0; workerIndex < nm_of_threads; workerIndex++) {
        auto p = new K_means_memory(k, dim);
        thread_data.push_back(p);
        // create slave threads, assign them their kmeans memory objects, barrier reference and main memory pointers
        workers.push_back(std::thread(compute_k_means_parallel_slave, &points, left, right, delegated,
                                      thread_data[workerIndex], ref(barrier), workerIndex));
        left = right; // limits of points slave should take care of
        if (workerIndex== nm_of_threads-2) right = points.number_of_points;
        else right = std::min(points.number_of_points, right + step);
    }

    for (int epoch = 0; epoch < epochs; epoch++) { // same as in single threaded variant


        // k++ like initialization
        /*
        for (int i = 0; i < k; i++){
            agg_p->reset_memory();
            barrier.wait_master();
            if (i==0) initialize_point_by_random(i, *mem_p, points);
            else {
                for (auto td : thread_data)  agg_p->combine_second_mem(*td);
                initialize_point_from_mem(i, agg_p->get_most_distant_point(), *mem_p, points);
            }
            mem_p->set_lim(i+1);
            barrier.wake_slaves(false);
        }
        */

        //random initialization
        barrier.wait_master();
        for (int i = 0; i < k; i++){
            initialize_point_by_random(i, *mem_p, points);
        }
        mem_p->set_lim(k);
        barrier.wake_slaves(false);


        for (int cycle = 0; cycle < cycles; cycle++) {
            agg_p->reset_memory();
            barrier.wait_master(); // wait until slaves finish their work

            for (auto td : thread_data) agg_p->combine_second_mem(*td);

            int to_init = 0;
            for (int i = 0; i < k; i++) {
                auto b = agg_p->average_accumulated(i, bound);
                //reinitialize point that is mean to small number of data
                if (!b) {
                    //if (to_init == 0) initialize_point_from_mem(i, agg_p->get_most_distant_point(), *agg_p, points);
                    //else initialize_point_by_random(i, *agg_p, points);
                    initialize_point_by_random(i, *agg_p, points);
                    to_init++;
                }
            }
            auto error = mem_p->get_max_change(*agg_p);
            auto tmp = mem_p;
            mem_p = agg_p;
            agg_p = tmp;
            //change pointers to memory so workers has access to new means
            auto quit = (error < tolerance) || (cycle + 1 == cycles);
            barrier.wake_slaves(false); // wake slaves after new means are calculated
            if (quit) {
                break;
            }
        }

        if (mem_p->get_L2loss() < min_loss || epoch==0 ){
            final_mem.copy_data(*mem_p);
            min_loss = mem_p->get_L2loss();
        }
    }

    barrier.wait_master();
    mem_p = &final_mem;
    barrier.wake_slaves(true);  // wake slaves to end their threads

    for (int workerIndex = 0; workerIndex < nm_of_threads; workerIndex++) {
        workers[workerIndex].join();
        delete thread_data[workerIndex]; // free memory
    }

    return final_mem;
}

void initialize_point_by_random(int index, K_means_memory& mem, const PointMemory& p){
    // initialize point by picking random point in point memory
    static std::random_device rd;
    static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    static std::uniform_int_distribution<> distrib(0, p.number_of_points-1);
    auto point_index = distrib(gen);
    mem.set_ith_mean(index, *(p.points[point_index]));
}

void initialize_point_from_mem(int index, int worst_index, K_means_memory& mem, const PointMemory& p){
    // used to initialize mean by finding most distant point from current means
    mem.set_ith_mean(index, *(p.points[worst_index]));
}

void write_to_file(std::ostream & outfile, const PointMemory& p, const K_means_memory& k, int dim){
    // write to stream in a way that can be used in create image program
    outfile << p.number_of_points << " " << k.get_nm_of_classes() << " " <<  dim << std::endl;
    outfile << "Means:\n";
    for (int i = 0; i < k.get_nm_of_classes(); i++){
        outfile << (*k.get_point(i)) << endl;
    }
    outfile << "Points:\n";
    std::random_device rd;
    for (int i = 0; i < p.number_of_points; i++){
        outfile << *(p.points[i]) << " " << std::to_string(p.assignment[i]) << std::endl;
    }
}