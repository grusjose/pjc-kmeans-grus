#include "random"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

using namespace std;

vector<double> get_random_mean(int dim) {
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_real_distribution<> dist(-100, 100);
    vector<double> u(dim);
    for (int i = 0; i < dim; i++) u[i] = dist(mt);
    return u;
}

vector<double> get_random_std(int dim, int left, int right) {
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_real_distribution<> dist(left, right);
    vector<double> u(dim);
    for (int i = 0; i < dim; i++) u[i] = dist(mt);
    return u;
}

struct Point{
    vector<double> coords;
    int type;
    Point(const vector<double>& v, int t) : coords(v), type(t){};

    std::string get_string(){
        std::string s = std::to_string(coords[0]);
        for (int i = 1; i < coords.size();i++){
            s += " " + std::to_string(coords[i]);
        }
        return s;
    }
    std::string get_string_labeled(){
        return get_string() + " " + std::to_string(type);
    }
};

vector<double> get_scaled_random(vector<double>& mean, vector<double>& sttd) {
    static std::mt19937 mt{ std::random_device{}() };
    static std::normal_distribution<double> dt;
    vector<double> u(mean.size());
    for (int i = 0; i < mean.size(); i++) u[i] = sttd[i] * dt(mt) +mean[i];
    return u;
}

double get_dist(const vector<double>& a, const vector<double>& b){
    double val = 0;
    for (int i = 0; i < a.size(); i++){
        val += pow(a[i] - b[i], 2);
    }
    return sqrt(val);
}

struct DistributionParameters{
    int left, right;
    vector<double> u;
    vector<double> s;
    int dim;
    //uses only diagonal covarinace matrix
    explicit DistributionParameters(const std::vector<DistributionParameters>& others, int d, int l, int r) : dim(d), left(l), right(r){
        while (true){
            auto ux = get_random_mean(dim);
            bool b = true;
            for (auto &d : others){
                if (get_dist(ux, d.u) < 40){
                    b = false;
                    break;
                }
            }
            if (b) {
                u = ux;
                break;
            }
        }
        s = get_random_std(dim, left, right);
    }
    std::string get_string(){
        std::string str = std::to_string(u[0]);
        for (int i = 1; i < dim;i++) str += " " + std::to_string(u[i]);
        return str;
    }

    Point create_point_from_dist(int index){
        auto x = get_scaled_random(u, s);
        return Point(x, index);
    }
};



void print_help_page(char *program){
    cout << "Program for generating random data for k-means clustering\n";
    cout << endl << "Usage:" << endl;
    cout << "\t" << program << " [options] OUTPUT_NAME" << endl << endl;
    cout << "General options:" << endl;
    cout << "\t-C [x > 1]\tnumber of classes [normally 3]" << endl;
    cout << "\t-N [x > 0]\tnumber of samples [normally 250]" << endl;
    cout << "\t-D [x > 0]\tdimension of points [normally 2]" << endl;
    cout << "\t-l [x > 0]\tset lower bound on std [normally 5]" << endl;
    cout << "\t-r [x > 0]\tset upper bound on std [normally 16]" << endl;
    cout << "\t--help\tthis help" << endl;
    cout << "OUTPUT_NAME:" << endl;
    cout << "\tspecifies name (without extension)\n";
    cout << "\traw and labeled files are generated\n";
}


int main(int argc, char* argv[]){
    int k = 3, nm = 250, dim = 2;
    int left = 5; int right = 16;
    std::string s = "generated";
    if (argc < 2){
        cerr << "Not enough parameters, use --help\n";
        return 1;
    }
    bool added_file = false;
    for (int i = 1; i < argc; i++){
        string parg = argv[i];
        if (parg == "--help") {
            print_help_page(argv[0]);
            return 0;
        }
        if (i+1==argc){
            s = argv[argc-1];
            added_file=true;
        }
        else if (parg == "-C") {
            if (i + 1 < argc) {
                k = atoi(argv[++i]);
            } else {
                cerr << "Unspecified number of classes" << endl;
                return 1;
            }
        }
        else if (parg == "-D") {
            if (i + 1 < argc) {
                dim = atoi(argv[++i]);
            } else {
                cerr << "Unspecified dimension" << endl;
                return 1;
            }
        }
        else if (parg == "-N"){
            if (i+1 < argc){
                nm = atoi(argv[++i]);

            } else {
                cerr << "Unspecified number of classes" << endl;
                return 1;
            }
        }else if (parg == "-l"){
            if (i+1 < argc){
                left = atoi(argv[++i]);

            } else {
                cerr << "Unspecified left boundary" << endl;
                return 1;
            }
        }else if (parg=="-r"){
            if (i+1 < argc){
                right = atoi(argv[++i]);

            } else {
                cerr << "Unspecified left boundary" << endl;
                return 1;
            }
        }
        else {
            cerr << "Unknown parameter" << endl;
            return 1;
        }
    }
    if (!added_file){
        cerr<<"Missing filename\n";
        return 1;
    }
    if (left < 1 || right > 100 || left >= right || dim < 2) {
        cerr << "Wrong boundaries\n";
        return 1;
    }

    std::ofstream outfile_raw(s + ".txt");
    std::ofstream outfile_labeled(s + "_labels.txt");
    std::vector<DistributionParameters> distros;
    for (int i = 0; i < k; i++){
        DistributionParameters d(distros, dim, left ,right);
        distros.push_back(d);
    }
    //std::vector<Point> points;
    outfile_raw << nm << " " << dim << std::endl;
    outfile_labeled << nm << " " << k << " " << dim << std::endl;
    outfile_labeled << "Means:\n";
    for (auto &m : distros) outfile_labeled << m.get_string() << endl;
    outfile_labeled << "Points:\n";
    std::random_device rd;
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distrib(0, k-1);

    for (int i = 0; i < nm; i++){
        int cl = distrib(gen);
        auto p = distros[cl].create_point_from_dist(cl);
        outfile_raw << p.get_string() << std::endl;
        outfile_labeled << p.get_string_labeled() << std::endl;
    }
    outfile_raw.close();
    outfile_labeled.close();
    return 0;
}
