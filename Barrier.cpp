#include "Barrier.h"


    Barrier::Barrier(int slave_threads){
        in_barrier = 0;
        phase_cnt = 0;
        master_switch = false;
        quit_switch = false;
        nm = slave_threads;
    }

    bool Barrier::get_to_quit(){
        return quit_switch;
    }

    void Barrier::wake_slaves(bool to_quit){
        /*
         * increment phase cnt to enable slaves to continue, signal them
         */
        unique_lock<mutex> lk(mtx_slave);
        quit_switch = to_quit;
        phase_cnt++;
        cond_slave.notify_all();
        lk.unlock();
    }

    void Barrier::wait_master(){
        /*
         * wait until slaves finish their work
         */
        unique_lock<mutex> lk(mtx_master);
        while (!master_switch) cond_master.wait(lk);
        master_switch = false;
    }

    void Barrier::wait_slave() {
        int old_phase = phase_cnt; // take old phase cnt
        unique_lock<mutex> lk(mtx_slave); //acquire mtx
        if (++in_barrier == nm){  // if all other slaves hit the barrier, signal master
            in_barrier = 0;
            unique_lock<mutex> mlk(mtx_master); // acquire master lock and change its flag
            master_switch = true;
            cond_master.notify_one(); // signal master
            mlk.unlock();
        }
        while(phase_cnt == old_phase) cond_slave.wait(lk); // wait until master wake slaves

    }

    void Barrier::acquire_lock_slave(){
        mtx_slave.lock();
    }

    void Barrier::unlock_lock_slave(){
        mtx_slave.unlock();
    }
