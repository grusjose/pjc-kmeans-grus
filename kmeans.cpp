//
// Created by josef on 25.09.20.
//
#include "DataPoint.h"
#include "k_means_computation.h"
#include <vector>
#include <iostream>
#include <fstream>

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void print_help_page(char *program){
    cout << "Program for performing kmeans clustering of input data\n";
    cout << endl << "Usage:" << endl;
    cout << "\t" << program << " [options]" << endl << endl;
    cout << "General options:" << endl;
    cout << "\t-C [x > 1]\tnumber of classes to be clustered to (normally 3)" << endl;
    cout << "\t-c [x > 0]\tmaximum number of cycles in epoch (normally 100)" << endl;
    cout << "\t-e [x > 0]\tnumber of independent epoch with reinitialization (normally 3)" << endl;
    cout << "\t-t [x > 0]\tstopping criterion tolerance (normally 0.01)" << endl;
    cout << "\t-p [x > 0]\tset parallel variant with [x] threads (normally 3)" << endl;
    cout << "\t--input [PATH]\tspecify path to input file, otherwise use stdin [ex. --input /input.txt]" << endl;
    cout << "\t--output [PATH]\tspecify path to output file without extension, otherwise use stdout [ex. --output /output]" << endl;
    cout << "\t--help\tthis help" << endl;
    cout << "Explanation:\n";
    cout << "\tEpochs:\tEvery epoch, all means are reinitialized nad computation is performed until convergence or max. nm. of cycles\n";
    cout << "\tCycles:\tMaximum number of iterations in one epoch\n";
    cout << "\tTolerance:\tparameter of sum of absolute changes in means stopping criterion\n";
    cout << "Data are expected in format:\n";
    cout << "1st line:\t[NM_OF_SAMPLES] [DIM_OF_SAMPLES]\n";
    cout << "2->NM_OF_SAMPLES\tcoord_1 coord_2 ... coord_DIM_OF_SAMPLES\n";
}



int main(int argc, char** argv){
    std::ifstream f;
    int nm_of_threads = 3;
    int classes = 3, epochs = 3, cycles=100;
    float tolerance = 0.01;
    bool parallel = false;
    std::string  input = "";
    std::string output_name= "";

    // handle cmd line parameters

    for (int i = 1; i < argc; i++){
        string parg = argv[i];
        if (parg == "--help") {
            print_help_page(argv[0]);
            return 0;
        }
        if (parg == "--output") {
            if (i + 1 < argc) {
                output_name = argv[++i];
            } else {
                cerr << "Unspecified output" << endl;
                return 1;
            }
        }
        else if (parg == "-C") {
            if (i + 1 < argc) {
                classes = atoi(argv[++i]);
            } else {
                cerr << "Unspecified number of classes" << endl;
                return 1;
            }
        }
        else if (parg == "-p"){
            if (i+1 < argc){
                nm_of_threads = atoi(argv[++i]);
                parallel=true;
                if (nm_of_threads < 2){
                    cerr << "Not possible with 0 slave threads\n";
                    return 1;
                }

            } else {
                cerr << "Unspecified number of classes" << endl;
                return 1;
            }
        }
        else if (parg == "-t"){
            if (i+1 < argc){
                tolerance = atof(argv[++i]);
            } else {
                cerr << "Unspecified tolerance\n" << endl;
                return 1;
            }
        }
        else if (parg == "-e"){
            if (i+1 < argc){
                epochs = atoi(argv[++i]);
            } else {
                cerr << "Unspecified number of epochs\n" << endl;
                return 1;
            }
        }else if (parg == "-c"){
            if (i+1 < argc){
                cycles = atoi(argv[++i]);
            } else {
                cerr << "Unspecified number of cycles\n" << endl;
                return 1;
            }
        }else if (parg == "--input"){
        if (i+1 < argc){
            input = argv[++i];
        } else {
            cerr << "Unspecified input\n" << endl;
            return 1;
        }
    }
        else {
            cerr << "Unknown parameter" << endl;
            return 1;
        }
    }
    if (classes < 2 || tolerance < 0){
        cerr << "invalid numerical parameters\n";
        return 1;
    }

    int n, dim;
    std::string s;
    if (input.empty()){
        cin >> n; cin >> dim;
        std::getline(cin, s);
    }else{
        f.open(input);
        f >> n; f >> dim;
        std::getline(f, s);
    }
    PointMemory points(n, dim);
    K_means_memory* means;
    try {
        if (input.empty()){
            create_point_memory(points, cin, dim, false);
        }else{
            create_point_memory(points, f, dim, false);
        }
    } catch (std::runtime_error& r) {
        std::cout << r.what() << endl; return 1;
    }

    auto start = std::chrono::high_resolution_clock::now(); // start stopwatch after loading
    if (parallel){
        // start parallel variant
        means = new K_means_memory(compute_k_means_parallel_master(points, classes, dim, cycles, epochs, tolerance, nm_of_threads));
    }else{
        // start single threaded variant
        means = new K_means_memory(compute_k_means_single_threaded(points, classes, dim, cycles, epochs, tolerance));
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::cerr << "Needed " << to_ms(end - start).count() << " ms to finish computation, saving.\n";
    if (output_name.empty()){
        write_to_file(std::cout, points, *means, dim);
    }else{
        ofstream o;
        o.open(output_name + ".txt");
        write_to_file(o, points, *means, dim);
        o.close();
    }
    delete means;
    return 0;
}
