**Josef Grus - KMEANS - PJC**

**SHA hash** = 6279aa54f3be6698297a70dacbc2d2890cbdfcf3

**Popis zadání**

Úkolem bylo vytvořit program pro řešení shlukování metodou K průměrů (K-means). Pro zvolené číslo k představující počet tříd, do kterých se mají neznámá neoznačená data rozdělit, je inicializovaná množina průměrů. Neznámé body jsou přiřazeny k nejbližšímu průměru. Průměry jsou dále přepočteny tak, aby ležely v aritmetickém průměru jim přiřazených bodů. Tento postup se opakuje do konvergence, tedy dokud se posouvají jednotlivé průměry.

Pro vizualizaci výsledků slouží pomocný program, který pro 2D data vytvoří barevné SVG obrázky. Další pomocný program pak slouží pro generování náhodných dat.

**Popis implementace**

Pro zjednodušení práce s výsledným programem byly kromě hlavního programu řešícího samotné shlukování vytvořeny utility pro generování náhodných dat a vizualizaci výsledku do obrázku.

**generate_data**

Program na základě vstupních parametrů vygeneruje množinu bodů dané dimenzionality. Data jsou generována v příslušné hyperkrychli o rozměrech -100, 100 v každé dimenzi. Matice kovariance je pro jednoduchost diagonální, její parametry jsou náhodná čísla ležící ve zvoleném intervalu. Kromě neolabelovaných dat je vytvořen též soubor s přidělenými značkami pro případnou vizualizaci ground truth hodnot.

Příklad použití: ./generate_data -C 5 -N 1000 data       

- vytvoří data.txt soubor s 1000 vzorků z pěti různých normálních rozdělení s normálními limity pro matici kovariance. Rovněž jako data_labels.txt vznikne vzorové rozdělení pro vizualizaci.

**create_image**

Vytvoří ze souboru obsahujícího označená data .svg obrázek. Body jsou označeny barevně, bíle jsou zvýrazněny pozice příslušných středních hodnot. Pro tvorbu SVG obrázků se použila knihovna SIMPLE SVG library z https://github.com/adishavit/simple-svg/blob/master/simple_svg_1.0.0.hpp.

Příklad použití: ./create_image data_labels.txt output     

- vytvoří obrázek output.svg ze vstupního souboru.

**k_means**

Hlavní program pro provedení samotného shlukování. Po načtení dat ze souboru nebo standardního vstupu je opakovaně prováděna operace popsaná v první části textu. Každý bod v paměti je porovnán s aktuálními uloženými průměry, je provedeno jeho přiřazení a bod je přičten do akumulující paměti příslušné nejbližšímu průměru. Poté jsou pomocí těchto akumulací přepočítány hodnoty průměrů.  

Inicializace počátečních průměrů je provedena náhodným výběrem bodů z paměti. Experimentoval jsem s variací na kmeans++ inicializaci, ale nedařilo se mi zprovoznit randomizovaný přístup. Při nekvalitní úvodní inicializaci mohou průměry skončit na nevhodných pozicích. Pro nalezení optimálního rozložení je možné provádět vyšší počet epoch (přepínač -e) s různými inicializacemi, kdy je ve výsledku  uložena konfigurace minimalizující eukleidovskou vzdálenost od svých bodů. Počet iterací vnitřního cyklu je omezen maximálním počtem cycles -c, nebo je běh předčasně ukončen, když je suma posunutí průměrů menší než tolerance daná přepínačem -t. 

Vícevláknový program paralelizuje výpočet tak, že rozdělí načtené body mezi jednotlivá slave vlákna. Poté, co slave vlákna provedou přiřazení a akumulaci bodů, master vlákno sečte příspěvky jednotlivých slave vláken a určí nové hodnoty průměrů. Poté opět signalizuje slave vláknům, aby provedly nový výpočet. Pro synchronizaci jsem použil bariéry, viz komentáře v kódu. Poté, co všechna slave vlákna dokončí svůj výpočet, signalizuje poslední takové vlákno mastera a čeká spolu s ostatními na signál master vlákna pro znovuzahájení výpočtu. 

Použitá implementace paralelního běhu neefektivně využívá jedno (master) vlákno, která při výpočtu přiřazení je v režimu IDLE a provádí pouze časově relativně nenáročný výpočet zkombinování výsledků slave vláken. Lepším řešením by bylo nechat i master vlákno pracovat na části množiny bodů, což se mi bohužel nepodařilo naimplementovat, mimojiné kvůli neschopnosti řešit reinicializaci průměrů během iteraci nebo na začátku nové epochy bez nutnosti ukončit aktivity ostatních vláken. Z toho důvodu je běh se 2 vlákny (1 master a 1 slave) pomalejší než single-threaded, protože navíc obsahuje časové ztráty spojené se synchronizací. Běhy s vyšším počtem slave vláken již dosahoval 

Byly provedeny dva experimenty pro zkoumání vlivu počtu vláken na rychlost vykonání programu. Prvni experiment ukazuje dobu výpočtu dle počtu vstupních bodů dimenze 2, navíc s počtem epoch 10. Hodnoty se získaly průměrováním 5 experimentů pro stejné nastavení. Je patrné, že výpočet probíhal rychle i pro velké počty bodů, důvodem také bylo, že výpočet obvykle konvergoval do 30 iterací (cycles). V druhém experimentu jsem pro vynucení delší doby běhu programu nastavil toleranci -t 0, čímž se zajistí běh každé epochy pro maximální počet iterací. Dalším nastavením bylo -e 10 a -c 250. Výsledky jsou k vidění na obrázku. Experimentální data jsou v ms uvedena v tabulkách. Měření jsem provedl v Ubuntu 18.04 s procesorem Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz s 8 GB RAM. Z dat je patrné, že kromě případu se dvěma vlákny, která jsou v implementaci principiálně rovna jednomu vláknu, je běh vícevláknové aplikace rychlejší. Experimenty byly provedeny s kompilací v release módu.

Vzorová vstupní data jsou example...txt. N odpovídá počtu vzorků, C je počet tříd. Výsledné obrázky jsou také součástí repozitáře.

Pozn. počet vláken daný parametrem -p v případě vícevláknového běhu odpovídá počtu slave vláken (tj. lze očekávat, že běh s -p 1 bude mírně pomalejší než single-threaded varianta).

Tab - první experiment

| nm     | p=1  | p=2  | p=3 | p=4 |
|--------|------|------|-----|-----|
| 50000  | 50   | 70   | 30  | 20  |
| 100000 | 130  | 150  | 90  | 80  |
| 250000 | 240  | 250  | 140 | 100 |
| 500000 | 1100 | 1200 | 640 | 560 |


Tab - druhý experiment


| nm     | p=1  | p=2  | p=3  | p=4  |
|--------|------|------|------|------|
| 1000   | 50   | 70   | 60   | 55   |
| 10000  | 420  | 471  | 310  | 300  |
| 20000  | 860  | 940  | 600  | 500  |
| 50000  | 2240 | 2300 | 1500 | 1200 |
| 100000 | 4460 | 4700 | 2900 | 2500 |

Příklad použití:
pro zákládní použití stačí zvolit vhodný počet clusterů přepínačem -C a případně počet vláken pro paralelní režim, plus cesty pro vstup a výstup, případně tyto poskynout na stdin a stdout. Příkladné volání pak je:

./k_means --input data.txt --output out -C 5 -p 4            

- provede  K-means clustering pro 5 clusterů, pro vstup data.txt, výstup uloží do out.txt, provede v paralelním režimu za použití 4 vláken (absence přepínače -p provede výpočet v sériovém režimu).
