#ifndef SEM_DATAPOINT_H
#define SEM_DATAPOINT_H

#include <mutex>
#include <condition_variable>
#include <vector>
#include <fstream>
#include <tuple>
#include <istream>
#include <ostream>

class Point {
private:
    std::vector<double> coords;
    int dim;
public:
    Point() = default;
    explicit Point(const Point&);
    explicit Point(int);
    explicit Point(const std::vector<double> &);
    ~Point() = default;

    bool copy_coords(const std::vector<double> &);
    bool copy_coords(const Point &);
    void reset_point();
    double compute_distance(const Point &) const;
    double compute_norm() const;
    double get_coord(int) const;
    Point &operator+=(const Point &rhs);
    Point &operator*=(const double&);

    friend std::ostream & operator<<(std::ostream& out, const Point& p);
};

class PointMemory{
public:
    int number_of_points, current_point;
    std::vector<Point*> points;
    std::vector<int> assignment;
    PointMemory(int, int);
    ~PointMemory();
    bool add_point(const std::vector<double>&);
};

class K_means_memory{
protected:
    int nm_of_classes;
    int lim;
    int most_distant_point;
    double most_distant_val;
    double L2loss;
    std::vector<int> agg_cnt;
    std::vector<Point*> k_means;
public:
    K_means_memory(int, int);
    K_means_memory(const K_means_memory&);
    K_means_memory& operator=(const K_means_memory&);
    ~K_means_memory();
    void copy_data(const K_means_memory& rhs);
    void reset_memory();
    void check_new_dist(const double &, const int&);
    std::pair<int, double> assign_point_to_class(const Point*) const;
    bool average_accumulated(int, int);
    void set_ith_mean(int, const Point&);
    void update_k_means_values(int, const Point* p);
    int get_nm_of_classes() const;
    int get_agg_num(int) const;
    double get_L2loss() const;
    const Point* get_point(int) const;
    double get_max_change(const K_means_memory&) const;
    void combine_second_mem(const K_means_memory&);
    int get_most_distant_point() const;
    void set_lim(int lim);
};

void assign_points_and_accumulate_coords(const K_means_memory& current, K_means_memory& acc, PointMemory& points, int left, int right);

void create_point_memory(PointMemory& memory, std::istream & , int, bool);

std::ostream & operator<<(std::ostream& out, const Point& p);

#endif //SEM_DATAPOINT_H
