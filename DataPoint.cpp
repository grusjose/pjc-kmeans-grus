//
// Created by josef on 05.10.20.
//


#include "DataPoint.h"
#include <vector>
#include <iostream>
#include <math.h>
#include <istream>
#include <sstream>
#include <tuple>

Point::Point(int d) {
    /*
     * Represents point in d-dimensional space
     */
    dim = d;
    coords = std::vector<double>(d, 0);
}

Point::Point(const std::vector<double> &c) {
    coords = c;
    dim = c.size();
}

Point::Point(const Point& rhs) {
    dim = rhs.dim;
    coords = std::vector<double>(dim, 0);
    copy_coords(rhs);
}

double Point::get_coord(int index) const {
    if (index > dim) return 0;
    return coords[index];
}

bool Point::copy_coords(const std::vector<double> &vec) {
    if (dim != vec.size()) {
        return false;
    }
    for (int i = 0; i < dim; i++) {
        coords[i] = vec[i];
    }
    return true;
}

bool Point::copy_coords(const Point & p) {
    if (dim != p.dim) {
        return false;
    }
    for (int i = 0; i < dim; i++) {
        coords[i] = p.coords[i];
    }
}

void Point::reset_point() {
    for (int i = 0; i < dim; i++) coords[i] = 0.;
}

Point & Point::operator+=(const Point &rhs){
    for (int i = 0; i < rhs.dim; i++){
        coords[i] += rhs.coords[i];
    }
}
Point & Point::operator*=(const double& d){
    for (int i = 0; i < dim; i++){
        coords[i] *= d;
    }
}

double Point::compute_distance(const Point & rhs) const{
    double acc = 0;
    for (int i = 0; i < dim; i++) acc += std::pow(coords[i] - rhs.coords[i], 2);
    return std::sqrt(acc);
}

double Point::compute_norm() const {
    double acc = 0;
    for (int i = 0; i < dim; i++) acc += std::pow(coords[i], 2);
    return acc;
}

PointMemory::PointMemory(int nm, int dim) {
    /*
     * Main memory storing loaded points, distributed among all threads
     */
    number_of_points = nm; current_point = 0;
    points = std::vector<Point*>(nm, nullptr);
    for (auto& p : points) p = new Point(dim);
    assignment = std::vector<int>(nm, -1);
}

PointMemory::~PointMemory() {
    for (auto p : points) delete p;
}

bool PointMemory::add_point(const std::vector<double> &vec) {
    if (current_point == number_of_points) return false;
    points[current_point]->copy_coords(vec);
    current_point++;
    return true;
}

K_means_memory::K_means_memory(int k, int dim) {
    /*
     *  Stores current k means, every thread controls its memory, its assembled by master thread
     */
    nm_of_classes = k;
    k_means = std::vector<Point*>(k, nullptr);
    agg_cnt = std::vector<int>(k, 0);
    most_distant_point = 0;
    most_distant_val = 0;
    lim = nm_of_classes;
    L2loss = 0;
    for (auto& p : k_means) p = new Point(dim);
}

K_means_memory::~K_means_memory() {
    for (auto p: k_means) delete p;
}

void K_means_memory::reset_memory() {
    for (int i = 0; i < nm_of_classes; i++) {
        agg_cnt[i] = 0;
        k_means[i]->reset_point();
    }
    L2loss = 0;
    lim = nm_of_classes;
    most_distant_val = 0;
}

std::pair<int, double> K_means_memory::assign_point_to_class(const Point * p) const{
    /*
     * determine to which mean is chosen point closet -> create assignment
     */
    double min = k_means[0]->compute_distance(*p);
    int index = 0;
    for (int i = 1; i < lim; i++){
        auto new_dist = k_means[i]->compute_distance(*p);
        if (new_dist < min) {
            min = new_dist;
            index = i;
        }
    }
    std::pair<int, double> r(index, min);
    return r;
}

bool K_means_memory::average_accumulated(int index, int bound) {
    /*
     * divide accumulated means by number of points assigned to the class
     */
    if (agg_cnt[index] > bound){
        *(k_means[index]) *= 1. / ((double)agg_cnt[index]);
        return true;
    }
    return false;
}

void K_means_memory::set_ith_mean(int i, const Point & p) {
    k_means[i]->copy_coords(p);
}

void K_means_memory::update_k_means_values(int class_nm, const Point* p){
    /*
     * add new point to class based on assignment
     */
    agg_cnt[class_nm]++;
    *(k_means[class_nm]) += *p;
}

int K_means_memory::get_nm_of_classes() const{
    return nm_of_classes;
}

const Point* K_means_memory::get_point(int index) const{
    return k_means[index];
}

double K_means_memory::get_max_change(const K_means_memory& rhs) const{
    /*
     * compute stopping criterion == combines maximum distances for all k means
     */
    double max_change = 0;
    for (int i = 0; i < nm_of_classes; i++){
        auto p = rhs.get_point(i);
        auto change = k_means[i]->compute_distance(*p);
        max_change += change;
    }
    return max_change;
}

int K_means_memory::get_agg_num(int index) const{
    return agg_cnt[index];
}

void K_means_memory::check_new_dist(const double& d, const int& i) {
    L2loss += d;
    if (d > most_distant_val) {
        most_distant_val = d;
        most_distant_point = i;
    }
}

void K_means_memory::combine_second_mem(const K_means_memory &rhs) {
    /*
     * add accumulated data from slave threads to memory of master thread
     */
    for (int i = 0; i < nm_of_classes; i++){
        *(k_means[i]) += *(rhs.k_means[i]);
        agg_cnt[i] += rhs.agg_cnt[i];
    }
    if (rhs.most_distant_val > most_distant_val){
        most_distant_val = rhs.most_distant_val;
        most_distant_point = rhs.most_distant_point;
    }
    L2loss += rhs.L2loss;
}

int K_means_memory::get_most_distant_point() const {
    return most_distant_point;
}

void K_means_memory::set_lim(int l=-1) {
    // outdated, used to initialize points with kmeans++ like method, currently using random initialization
    if (l==-1) lim = nm_of_classes;
    lim = l;
}

K_means_memory::K_means_memory(const K_means_memory &rhs){
    most_distant_point = 0; most_distant_val = 0;
    nm_of_classes = rhs.nm_of_classes; lim = rhs.lim;
    agg_cnt = std::vector<int>(nm_of_classes,0);
    k_means = std::vector<Point*>(nm_of_classes, nullptr);
    for (int i = 0; i < nm_of_classes; i++){
        k_means[i] = new Point(*rhs.k_means[i]);
    }
    L2loss = 0;
}

double K_means_memory::get_L2loss() const {
    return L2loss;
}


void K_means_memory::copy_data(const K_means_memory &rhs) {
    for (int i = 0; i < nm_of_classes; i++) {
        k_means[i]->copy_coords(*rhs.k_means[i]);
    }
}


K_means_memory& K_means_memory::operator=(const K_means_memory& rhs){
    for (auto p: k_means) delete p;
    most_distant_point = 0; most_distant_val = 0;
    nm_of_classes = rhs.nm_of_classes; lim = rhs.lim;
    agg_cnt = std::vector<int>(nm_of_classes,0);
    k_means = std::vector<Point*>(nm_of_classes, nullptr);
    for (int i = 0; i < nm_of_classes; i++){
        k_means[i] = new Point(*rhs.k_means[i]);
    }
    L2loss = 0;
    return *this;
}

void assign_points_and_accumulate_coords(const K_means_memory& current, K_means_memory& acc, PointMemory& points, int left, int right){
    /*
     *  assign set of given points to their means, accumulate data for next iteration means and error
     */
    acc.reset_memory();
    for (int i = left; i < right; i++){
        Point* p = points.points[i];
        auto assignment = current.assign_point_to_class(p);
        auto index = std::get<0>(assignment);
        auto min = std::get<1>(assignment);
        points.assignment[i] = index;
        acc.check_new_dist(min, i);
        acc.update_k_means_values(index, p);
    }
}

void create_point_memory(PointMemory& memory, std::istream & input, int dim, bool annotation){
    /*
     * load data from stream to point memory
     */
    std::vector<double> vec(dim);
    int n = memory.number_of_points;
    double s;
    std::string ss;
    for (int i = 0; i < n; i++) {
        std::istringstream stream;
        try {

            std::getline(input, ss);
            stream = std::istringstream(ss);
            for (int j = 0; j < dim; j++) {
                std::string sa;
                stream >> sa;
                vec[j] = std::stod(sa);
            }
        } catch (std::exception& e) {
            throw std::runtime_error("wrong data loading");
        }
        if (annotation){
            std::string a;
            stream >> a;
            memory.assignment[i] = std::stoi(a);
        }
        auto b = memory.add_point(vec);
    }
}

std::ostream & operator<<(std::ostream& out, const Point& p){
    for (int i = 0; i < p.dim; i++){
        out << p.coords[i];
        if (i < p.dim-1) out << " ";
    }
    return out;
}