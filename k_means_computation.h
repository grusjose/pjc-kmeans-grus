#ifndef SEM_K_MEANS_SINGLE_THREAD_H
#define SEM_K_MEANS_SINGLE_THREAD_H

#include "k_means_computation.h"
#include "DataPoint.h"
#include <random>
#include <string>
#include <ostream>
#include "Barrier.h"

K_means_memory compute_k_means_single_threaded(PointMemory& points, int k, int dim, int cycles, int epochs, double tolerance);
void initialize_point_by_random(int index, K_means_memory& mem, const PointMemory& p);
void initialize_point_from_mem(int index, int worst_index, K_means_memory& mem, const PointMemory& p);
K_means_memory compute_k_means_parallel_master(PointMemory& points, int k, int dim, int cycles, int epochs, double tolerance, int nm_of_threads);
void compute_k_means_parallel_slave(PointMemory* points, int left, int right, K_means_memory** current_mem,
                                    K_means_memory* agg_mem, Barrier& barrier, int idx);
void write_to_file(std::ostream& file_name, const PointMemory& p, const K_means_memory& k, int);
#endif //SEM_K_MEANS_SINGLE_THREAD_H
